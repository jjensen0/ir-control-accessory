/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <util/twi.h>

#define IRPIN 	PB1
#define IRPORT 	PINB
#define IRDDR 	DDRB
#define LEDPIN 	PC2
#define LEDPORT PORTC
#define LEDDDR 	DDRC
#define DEPORT 	PORTD
#define DEPIN 	PD5
#define DEDDR 	DDRD

#define TOGGLE_PIN(port, pos) 		port ^= (1<<pos)

#define TWI_SCL						PC5
#define TWI_SDA 					PC4
#define TWI_STX_ADR_ACK            	0xA8  // Own SLA+R has been received; ACK has been returned
#define TWI_STX_ADR_ACK_M_ARB_LOST 	0xB0  // Arbitration lost in SLA+R/W as Master; own SLA+R has been received; ACK has been returned
#define TWI_STX_DATA_ACK           	0xB8  // Data byte in TWDR has been transmitted; ACK has been received
#define TWI_STX_DATA_NACK          	0xC0  // Data byte in TWDR has been transmitted; NOT ACK has been received
#define TWI_STX_DATA_ACK_LAST_BYTE 	0xC8  // Last data byte in TWDR has been transmitted (TWEA = �0�); ACK has been received
#define IRHIGH						((IRPORT & (1<<IRPIN)) == (1<<IRPIN))
#define IRLOW 						!IRHIGH

volatile unsigned char data = 0x00;
volatile unsigned char olddata = 0x00;
volatile unsigned char sentStop = 0x01;
volatile unsigned char inProgress = 0x00;

ISR(TWI_vect) {
	switch (TWSR ) {
	case TWI_STX_ADR_ACK:
		if(!inProgress)
		{
			TWDR = olddata;
			TWCR |= (1 << TWINT);
			inProgress++;
		}
		break;
	case TWI_STX_DATA_ACK:
	case TWI_STX_DATA_NACK:
		TWCR |= (1 << TWINT);
		inProgress = 0x00;
		break;
	default:
		TWCR |= (1 << TWINT) | (1 << TWSTO);
		break;
	}
	return;
}

ISR(TIMER1_OVF_vect)
{
	TCCR1B = 0;					// Turn off timer1
	TCNT1 = 36785;				// Reset starting count
	if (data == olddata && sentStop != 0x01) {
		olddata = 0xff;			// Load stop byte into old data
		sentStop = 0x01;
		TOGGLE_PIN(DEPORT, DEPIN);	// Signal for retrieval.
	}

	TCCR1B |= (1 << CS11);		// Re-enable timer 1, prescaler of 8
}

int main()
{
	// Init ports
	DEDDR |= (1 << DEPIN); // Data read enable output pin
	DEPORT &= ~(1 << DEPIN);
	DDRC |= (1 << PC2) | (1 << PC4);		//I2C SDA output.
	DDRD |= (1 << PD2);					//Debug instrumentation
	//Init TWI
	TWAR = 0x54; // 0b0101 0100 - Address chosen 0x26 (LSB bit is disregarded)
	TWCR =  (1 << TWEN) |                             // TWI Interface enabled.
			(1 << TWIE) | (1 << TWINT) | // Enable TWI Interrupt and clear the flag.
			(1 << TWEA) | (0 << TWSTA) | (0 << TWSTO) | // Prepare to ACK next time the Slave is addressed.
			(0 << TWWC);
	//Init timer 1
	TIMSK1 |= (1 << TOIE1);				// Enable timer 1 interrupts.
	while (1) {
		sei();
		while (IRHIGH)
			;
		PORTD |= (1 << PD2);
		_delay_us(1000); // Sample after 1ms to determine if still start...

		if (IRHIGH) // Invalid code, random noise
			continue;
		//cli();
		data = 0x00;

		while (!IRHIGH);

		PORTD &= ~(1 << PD2);

		while (IRHIGH);

		for (int i = 0; i < 12; i++)
		{
			_delay_us(800);

			if (i < 7) //Disregard anything after the command bits (Sony protocol)
			{
				if (!IRHIGH) // This is a logical 1 (active low)
					data |= (1 << i);	// enter next bit and shift it (LSB first)
				else
					data &= ~(0 << i);

				// if the last bit coming in was 1, wait until IR sensor goes idle again
				if ((data & (1 << i)) == (1 << i))
					while (!IRHIGH);	// while data

				// wait until IR sensor gets activated again.
				while (IRHIGH);
			}

			else
			{
				while (!IRHIGH)
					;
				if (i != 11)
					while (IRHIGH)
						;
			}
		}
		sei();
		olddata = data;
		sentStop = 0x00;
		TCCR1B = 0;
		TCNT1 = 36785;
		if(!inProgress)					// Only do this if there isn't one already going
			TOGGLE_PIN(DEPORT, DEPIN);
		TCCR1B |= (1<<CS11);		// Enable timer at no prescaler.
		_delay_ms(20);				// Error correction, waiting 20MS shouldn't affect much, unless it's an error.
	}
}
