/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "capi324v221.h"
#include <avr/interrupt.h>

#define TOGGLE_PIN(port, pos) port ^= (1<<pos)
unsigned char data = 0xFF;
volatile uint8_t newData = 0x00;
volatile unsigned char error = 0x00;

CBOT_ISR(pinChangeInterupt);

void CBOT_main() {
	DDRA &= ~(1 << PA4);		// Data Ready Interupt
	DDRA |= (1 << PA3);			// Troubleshooting tool
	PCICR |= (1 << PCIE0);		//Enable Pin Change interrupt PCINT0
	PCMSK0 |= (1 << PCINT4);		// Enable PA4 to cause interrupt.
	uint8_t speed = 100;
	ADC_open();
	LCD_open();
	LCD_clear();
	I2C_open();
	I2C_pullup_enable();
	I2C_set_BRG(0x5C, I2C_PRESCALER_1);
	STEPPER_open();
	SPKR_open(SPKR_BEEP_MODE);
	ISR_attach(ISR_PCINT0_VECT, pinChangeInterupt);
	sei();
	BATTERY_check();
	while (1) {
		if (!error) {
			LCD_printf("0x%02xh\t"
					"Speed: %d\t\t\t", data, speed);
			if (newData) {
				switch (data) {
				case 0x00:			//Numpad 1
					//Go forward and to the left 45 degree's.
					STEPPER_move(STEPPER_STEP_NO_BLOCK, STEPPER_BOTH,
							STEPPER_FWD, 300, speed * 1.5, 400, STEPPER_BRK_OFF,
							NULL, // left
							STEPPER_FWD, 300, speed * 2, 400, STEPPER_BRK_OFF,
							NULL ); // right
					break;
				case 0x01:			//Numpad 2
					//Go forward
					STEPPER_run(STEPPER_BOTH, STEPPER_FWD, speed * 2);
					break;
				case 0x02:			//Numpad 3
					//Forward and to the right..
					STEPPER_move(STEPPER_STEP_NO_BLOCK, STEPPER_BOTH,
							STEPPER_FWD, 300, speed * 2, 400, STEPPER_BRK_OFF,
							NULL, // left
							STEPPER_FWD, 300, speed * 1.5, 400, STEPPER_BRK_OFF,
							NULL ); // right
					break;
				case 0x03:			//Numpad 4
					//90 degree turn left and forward.
					STEPPER_move(STEPPER_STEP_NO_BLOCK, STEPPER_BOTH,
							STEPPER_REV, 120, speed * 2, 400, STEPPER_BRK_OFF,
							NULL, // left
							STEPPER_FWD, 120, speed * 2, 400, STEPPER_BRK_OFF,
							NULL ); // right
					break;
				case 0x15:			//Power Button
					STEPPER_stop(STEPPER_BOTH, STEPPER_BRK_OFF);
					break;
				case 0x05:			//numpad 6
					//90 degree's right
					STEPPER_move(STEPPER_STEP_NO_BLOCK, STEPPER_BOTH,
							STEPPER_FWD, 120, speed * 2, 400, STEPPER_BRK_OFF,
							NULL, // left
							STEPPER_REV, 120, speed * 2, 400, STEPPER_BRK_OFF,
							NULL ); // right
					break;
				case 0x06:			// Numpad 7
					//45 degree's right and go back
					STEPPER_move(STEPPER_STEP_NO_BLOCK, STEPPER_BOTH,
							STEPPER_REV, 60, speed * 1.5, 400, STEPPER_BRK_OFF,
							NULL, // left
							STEPPER_REV, 60, speed * 2, 400, STEPPER_BRK_OFF,
							NULL ); // right
					break;
				case 0x07:			//Numpad 8
					//Straight back
					STEPPER_run(STEPPER_BOTH, STEPPER_REV, speed * 2);
					break;
				case 0x08:			//Numpad 9
					// 45 degree's left and back
					STEPPER_move(STEPPER_STEP_NO_BLOCK, STEPPER_BOTH,
							STEPPER_REV, 60, speed * 2, 400, STEPPER_BRK_OFF,
							NULL, // left
							STEPPER_REV, 60, speed * 1.5, 400, STEPPER_BRK_OFF,
							NULL ); // right
					break;
				case 0x04:			//Numpad 5
					//Honk
					SPKR_beep(440);
					break;
				case 0xff:			// Stop byte
					STEPPER_stop(STEPPER_BOTH, STEPPER_BRK_OFF);
					SPKR_beep(0);
					break;
				case 0x12:					//Volume UP
					if (++speed > 150)
						speed = 150;
					SPKR_beep(100 + speed);
					break;
				case 0x13:					//Volume Down
					if (--speed < 1)
						speed = 1;
					SPKR_beep(100 + speed);
					break;
				default:			//Power Button
					STEPPER_stop(STEPPER_BOTH, STEPPER_BRK_OFF);
					break;
				}
				newData = 0x00;
			}
		}
	}
	while (1)
		;
}

CBOT_ISR(pinChangeInterupt) {
	I2C_STATUS status = I2C_MSTR_start(0x2A, I2C_MODE_MR);
	if (status == I2C_STAT_OK) {
		error= 0x00;
		I2C_MSTR_get(&data, FALSE);
		I2C_MSTR_stop();
		newData = 1;
	} else {
		LCD_clear();
		I2C_MSTR_stop();
		STEPPER_close();		// Error? Stop all Stepper stuff
		SPKR_beep(0);// Stop all speaker events (prevents annoying beep while error)
		LCD_printf("Status: %d", status);
		error = 0x01;
		sei();
	}
}

